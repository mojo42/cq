/*
 * Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
 *
 * This work is open source software, licensed under the terms of the
 * BSD license as described in the LICENSE file in the top-level directory.
 */

CQ_FUNCTION(job1, void, int)
CQ_FUNCTION(job2, void, int)
