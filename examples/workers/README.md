A example of heterogeneous jobs pushed in a queue with several worker threads
consuming them.

To run the example:
```
$ make
$ ./workers
```
