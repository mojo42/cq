/*
 * Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
 *
 * This work is open source software, licensed under the terms of the
 * BSD license as described in the LICENSE file in the top-level directory.
 */

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

static void job1(int t)
{
    usleep(t);
    printf("job1 last %i µs in thread id %p\n", t, (void *) pthread_self());
}

static void job2(int t)
{
    usleep(t);
    printf("job2 last %i µs in thread id %p\n", t, (void *) pthread_self());
}

#define CQ_DESCRIPTION_FILE "desc.h"
#include "cq.h"

static void *worker(void *queue)
{
    cq_run_calls((struct cq *) queue);
    return NULL;
}

#define WORKERS 3

int main(void)
{
    int i;
    struct cq queue;
    cq_init(&queue);

    cq_push_call_no_res(&queue, job2, 4000);
    cq_push_call_no_res(&queue, job1, 100);
    cq_push_call_no_res(&queue, job1, 150);
    cq_push_call_no_res(&queue, job1, 100);
    cq_push_call_no_res(&queue, job1, 122);
    cq_push_call_no_res(&queue, job1, 133);
    cq_push_call_no_res(&queue, job2, 144);
    cq_push_call_no_res(&queue, job1, 123);
    cq_push_call_no_res(&queue, job2, 100);
    cq_push_call_no_res(&queue, job1, 192);
    cq_push_call_no_res(&queue, job2, 44);
    cq_push_call_no_res(&queue, job2, 2);

    pthread_t worker_threads[WORKERS];
    for (i = 0; i < WORKERS; i++)
        pthread_create(&worker_threads[i], NULL, &worker, (void *) &queue);

    for (i = 0; i < WORKERS; i++)
        pthread_join(worker_threads[i], NULL);

    cq_destroy(&queue);
    return 0;
}
