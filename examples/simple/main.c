/*
 * Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
 *
 * This work is open source software, licensed under the terms of the
 * BSD license as described in the LICENSE file in the top-level directory.
 */

#include <stdio.h>
#include "functions.h"

#define CQ_DESCRIPTION_FILE "desc.h"
#include "cq.h"

int main(void)
{
    int arg1 = 42;
    struct my_struct arg2 = {.i = 1, .j = 2};

    struct cq queue;
    cq_init(&queue);
    cq_push_call(&queue, function_a, 2);
    cq_push_call(&queue, function_b, 2, &arg1);
    cq_push_call(&queue, function_c, 2, &arg1, 10);
    cq_push_call(&queue, function_d);
    cq_push_call(&queue, function_e, 51);
    cq_push_call(&queue, function_f);
    cq_push_call(&queue, function_g, arg2);

    printf("let's run %lu jobs !\n", cq_job_count(&queue));
    cq_run_calls(&queue);
    printf("we now have %lu results !\n", cq_result_count(&queue));

    printf("result of function_a: %i\n", cq_result(&queue, int));
    cq_pop_result(&queue);
    printf("result of function_b: %i\n", cq_result(&queue, int));
    cq_pop_result(&queue);
    printf("result of function_c: %i\n", *(int *) cq_result_ptr(&queue));
    cq_pop_result(&queue);
    printf("result of function_d: %i\n", cq_result(&queue, int));
    cq_pop_result(&queue);
    printf("result of function_e\n");
    cq_pop_result(&queue);
    printf("result of function_f\n");
    cq_pop_result(&queue);
    printf("result of function_g: %i\n", cq_result(&queue, int));
    cq_pop_result(&queue);
    printf("no more results (%p)\n", cq_result_ptr(&queue));

    cq_destroy(&queue);
    return 0;
}
