/*
 * Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
 *
 * This work is open source software, licensed under the terms of the
 * BSD license as described in the LICENSE file in the top-level directory.
 */

#include <stdio.h>
#include "functions.h"

int function_a(int i)
{
    printf("running function_a\n");
    return i * 2;
}

int function_b(int i, int *j)
{
    printf("running function_b\n");
    return i * *j;
}

int function_c(int i, int *j, int k)
{
    printf("running function_c\n");
    return i / *j + k;
}

int function_d(void)
{
    printf("running function_d\n");
    return 42;
}

void function_e(int i)
{
    printf("running function_e (%i)\n", i);
}

void function_f(void)
{
    printf("running function_f\n");
}

int function_g(struct my_struct s)
{
    printf("running function_g\n");
    return s.i + s.j;
}
