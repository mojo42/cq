# Simple usage of CQ

- functions.c contains our functions
- desc.h contains description of our functions
- main.c push calls in a queue and consume queue

To run the example:
```
$ make
$ ./simple
```
