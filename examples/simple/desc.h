/*
 * Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
 *
 * This work is open source software, licensed under the terms of the
 * BSD license as described in the LICENSE file in the top-level directory.
 */

CQ_FUNCTION(function_a, int, int)
CQ_FUNCTION(function_b, int, int, int*)
CQ_FUNCTION(function_c, int, int, int*, int)
CQ_FUNCTION(function_d, int, void)
CQ_FUNCTION(function_e, void, int)
CQ_FUNCTION(function_f, void, void)
CQ_FUNCTION(function_g, int, struct my_struct)
