/*
 * Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
 *
 * This work is open source software, licensed under the terms of the
 * BSD license as described in the LICENSE file in the top-level directory.
 */

#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

struct my_struct {
  int i;
  int j;
};

int function_a(int i);
int function_b(int i, int *j);
int function_c(int i, int *j, int k);
int function_d(void);
void function_e(int i);
void function_f(void);
int function_g(struct my_struct s);

#endif /* #ifndef _FUNCTIONS_H */
