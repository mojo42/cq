/*
 * Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _CQ_H
#define _CQ_H

#include <stdlib.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/queue.h>

#ifndef CQ_DESCRIPTION_FILE
#error please define CQ_DESCRIPTION_FILE before including itc.h
#endif /* #ifndef CQ_DESCRIPTION_FILE */

/* Thanks to http://jhnet.co.uk/articles/cpp_magic page for helping ! */

#define CQ_CAT(a, va_args...) a ## va_args
#define CQ_INC(x) CQ_CAT(CQ_INC_, x)
#define CQ_INC_0 1
#define CQ_INC_1 2
#define CQ_INC_2 3
#define CQ_INC_3 4
#define CQ_INC_4 5
#define CQ_INC_5 6
#define CQ_INC_6 7
#define CQ_INC_7 8
#define CQ_INC_8 9
#define CQ_INC_9 10
#define CQ_INC_10 11
#define CQ_INC_11 12
#define CQ_INC_12 13
#define CQ_INC_13 14
#define CQ_INC_14 15
#define CQ_INC_15 16
#define CQ_INC_16 17
#define CQ_INC_17 18
#define CQ_INC_18 19
#define CQ_INC_19 20

#define CQ_FIRST(a, ...) a
#define CQ_SECOND(a, b, ...) b

#define CQ_EMPTY()

#define CQ_EVAL(...) CQ_EVAL1024(__VA_ARGS__)
#define CQ_EVAL1024(...) CQ_EVAL512(CQ_EVAL512(__VA_ARGS__))
#define CQ_EVAL512(...) CQ_EVAL256(CQ_EVAL256(__VA_ARGS__))
#define CQ_EVAL256(...) CQ_EVAL128(CQ_EVAL128(__VA_ARGS__))
#define CQ_EVAL128(...) CQ_EVAL64(CQ_EVAL64(__VA_ARGS__))
#define CQ_EVAL64(...) CQ_EVAL32(CQ_EVAL32(__VA_ARGS__))
#define CQ_EVAL32(...) CQ_EVAL16(CQ_EVAL16(__VA_ARGS__))
#define CQ_EVAL16(...) CQ_EVAL8(CQ_EVAL8(__VA_ARGS__))
#define CQ_EVAL8(...) CQ_EVAL4(CQ_EVAL4(__VA_ARGS__))
#define CQ_EVAL4(...) CQ_EVAL2(CQ_EVAL2(__VA_ARGS__))
#define CQ_EVAL2(...) CQ_EVAL1(CQ_EVAL1(__VA_ARGS__))
#define CQ_EVAL1(...) __VA_ARGS__

#define CQ_DEFER1(m) m CQ_EMPTY()
#define CQ_DEFER2(m) m CQ_EMPTY CQ_EMPTY()()
#define CQ_DEFER3(m) m CQ_EMPTY CQ_EMPTY CQ_EMPTY()()()
#define CQ_DEFER4(m) m CQ_EMPTY CQ_EMPTY CQ_EMPTY CQ_EMPTY()()()()

#define CQ_IS_PROBE(...) CQ_SECOND(__VA_ARGS__, 0)
#define CQ_PROBE() ~, 1

#define CQ_NOT(x) CQ_IS_PROBE(CQ_CAT(CQ__NOT_, x))
#define CQ__NOT_0 CQ_PROBE()

#define CQ_BOOL(x) CQ_NOT(CQ_NOT(x))

#define CQ_IF_ELSE(condition) CQ__IF_ELSE(CQ_BOOL(condition))
#define CQ__IF_ELSE(condition) CQ_CAT(CQ__IF_, condition)

#define CQ__IF_1(...) __VA_ARGS__ CQ__IF_1_ELSE
#define CQ__IF_0(...)             CQ__IF_0_ELSE

#define CQ__IF_1_ELSE(...)
#define CQ__IF_0_ELSE(...) __VA_ARGS__

#define CQ_HAS_ARGS(...) \
  CQ_BOOL(CQ_FIRST(CQ__END_OF_ARGUMENTS_ __VA_ARGS__)())
#define CQ__END_OF_ARGUMENTS_() 0

#define CQ_MAP(m, nb, type, ...) \
  m(nb, type) \
  CQ_IF_ELSE(CQ_HAS_ARGS(__VA_ARGS__)) \
    (CQ_DEFER2(CQ__MAP)()(m, CQ_INC(nb), __VA_ARGS__)) \
    ()
#define CQ__MAP() CQ_MAP

/* enumerate all function types */
#define CQ_ENUM_BUILD(function, ...) CQ_TYPE_##function,
enum cq_type
{
  #define CQ_FUNCTION CQ_ENUM_BUILD
  #include CQ_DESCRIPTION_FILE
  #undef CQ_FUNCTION
};

/* Build structure for each function */
#define CQ_STRUCT_NAME(function) function##_call
#define CQ_STRUCT_TYPE(pos, type) type arg##pos;
#define CQ_VOID_void
#define CQ_IS_VOID(e) CQ_NOT(CQ_HAS_ARGS(CQ_CAT(CQ_VOID_,e)))
#define CQ_STRUCT_BUILD(function, type_ret, types...) \
  struct CQ_STRUCT_NAME(function) \
  { \
    CQ_IF_ELSE(CQ_IS_VOID(type_ret))()(type_ret ret;) \
    CQ_IF_ELSE(CQ_IS_VOID(CQ_FIRST(types))) \
      () \
      (CQ_EVAL(CQ_MAP(CQ_STRUCT_TYPE, 0, types))) \
  };
#define CQ_FUNCTION CQ_STRUCT_BUILD
#include CQ_DESCRIPTION_FILE
#undef CQ_FUNCTION

/* union with all function strutures */
#define CQ_FP_SIGN_ARG(pos, type, ...) type arg##pos
#define CQ_FP_SIGN_BUILD(function, type_ret, types...) \
  struct CQ_STRUCT_NAME(function) function;

union cq_call
{
  #define CQ_FUNCTION CQ_FP_SIGN_BUILD
  #include CQ_DESCRIPTION_FILE
  #undef CQ_FUNCTION
};

/* call queue node */
struct cq_queue_node
{
  TAILQ_ENTRY(cq_queue_node) tailq;
  enum cq_type type;
  union cq_call call;
  int push_result;
};

TAILQ_HEAD(cq_tailq, cq_queue_node);

struct cq_queue
{
  struct cq_tailq queue;
  pthread_mutex_t lock;
  size_t size;
};

static inline void cq_queue_lock_(struct cq_queue *q)
{
  pthread_mutex_lock(&q->lock);
}

static inline void cq_queue_unlock_(struct cq_queue *q)
{
    pthread_mutex_unlock(&q->lock);
}

static inline int cq_queue_empty_(struct cq_queue *q)
{
  return TAILQ_EMPTY(&q->queue);
}

static inline struct cq_queue_node *cq_queue_next_(struct cq_queue *q)
{
  return TAILQ_LAST(&q->queue, cq_tailq);
}

static inline void cq_queue_free_(struct cq_queue *q)
{
  cq_queue_lock_(q);
  while (!cq_queue_empty_(q))
  {
    struct cq_queue_node *n = TAILQ_FIRST(&q->queue);
    TAILQ_REMOVE(&q->queue, n, tailq);
    free(n);
  }
  cq_queue_unlock_(q);
}

static inline void cq_queue_push_(struct cq_queue *q,
                                  struct cq_queue_node *n)
{
  cq_queue_lock_(q);
  TAILQ_INSERT_HEAD(&q->queue, n, tailq);
  q->size++;
  cq_queue_unlock_(q);
}

static inline struct cq_queue_node *cq_queue_pop_(struct cq_queue *q)
{
  struct cq_queue_node *n = NULL;
  cq_queue_lock_(q);
  if (!cq_queue_empty_(q))
  {
    n = TAILQ_LAST(&q->queue, cq_tailq);
    TAILQ_REMOVE(&q->queue, n, tailq);
    q->size--;
  }
  cq_queue_unlock_(q);
  return n;
}

static inline size_t cq_queue_size_(struct cq_queue *q)
{
  cq_queue_lock_(q);
  size_t size = q->size;
  cq_queue_unlock_(q);
  return size;
}

struct cq
{
  struct cq_queue jobs;
  struct cq_queue results;
};

static inline int cq_queue_init_(struct cq_queue *q)
{
  TAILQ_INIT(&q->queue);
  q->size = 0;
  return pthread_mutex_init(&q->lock, NULL);
}

static inline int cq_queue_destroy_(struct cq_queue *q)
{
  cq_queue_free_(q);
  return pthread_mutex_destroy(&q->lock);
}

static int cq_init(struct cq *context)
{
  return cq_queue_init_(&context->jobs) +
         cq_queue_init_(&context->results);
}

static int cq_destroy(struct cq *context)
{
  return cq_queue_destroy_(&context->jobs) +
         cq_queue_destroy_(&context->results);
}

/* Push calls in queue*/
#define CQ_MAP2(m, function, nb, type, ...) \
  m(function, nb, type) \
  CQ_IF_ELSE(CQ_HAS_ARGS(__VA_ARGS__)) \
    (CQ_DEFER2(CQ__MAP2)()(m, function, CQ_INC(nb), __VA_ARGS__)) \
    ()
#define CQ__MAP2() CQ_MAP2

#define CQ_PUSH_CALL_CONF_ARG(function, pos, ...) \
  n->call.function.arg##pos = va_arg(ap, typeof(n->call.function.arg##pos));
#define CQ_PUSH_CALL_BUILD(function, type_ret, types...) \
  if ((void *) func == (void *)&function) \
  { \
    n->type = CQ_TYPE_##function; \
      CQ_IF_ELSE(CQ_IS_VOID(CQ_FIRST(types))) \
        () \
        (CQ_EVAL(CQ_MAP2(CQ_PUSH_CALL_CONF_ARG, function, 0, types))) \
      goto end; \
  }

void cq_push_call_(struct cq *context, int push_result, void *func, ...)
{
  struct cq_queue_node *n = (struct cq_queue_node *)
    malloc(sizeof(struct cq_queue_node));
  va_list ap;
  va_start(ap, func);

  #define CQ_FUNCTION CQ_PUSH_CALL_BUILD
  #include CQ_DESCRIPTION_FILE
  #undef CQ_FUNCTION

  end:
  va_end(ap);
  n->push_result = push_result;
  cq_queue_push_(&context->jobs, n);
}

#define cq_push_call(context, ...) \
    cq_push_call_(context, 1, __VA_ARGS__)

#define cq_push_call_no_res(context, ...) \
    cq_push_call_(context, 0, __VA_ARGS__)

/* run calls from queue */
#define CQ_MAP_COMMA(m, function, nb, type, ...) \
  m(function, nb, type) \
  CQ_IF_ELSE(CQ_HAS_ARGS(__VA_ARGS__)) \
    (,CQ_DEFER2(CQ__MAP_COMMA)()(m, function, CQ_INC(nb), __VA_ARGS__)) \
    ()
#define CQ__MAP_COMMA() CQ_MAP_COMMA

#define CQ_RUN_CALL_ARG(function, pos, ...) n->call.function.arg##pos
#define CQ_RUN_CALL_BUILD(function, type_ret, types...) \
  case CQ_TYPE_##function: \
    CQ_IF_ELSE(CQ_IS_VOID(type_ret)) \
      ( \
        CQ_IF_ELSE(CQ_IS_VOID(CQ_FIRST(types))) \
          (function();) \
          (function(CQ_EVAL(CQ_MAP_COMMA(CQ_RUN_CALL_ARG, function, 0, types)));) \
      )( \
          CQ_IF_ELSE(CQ_IS_VOID(CQ_FIRST(types))) \
          (n->call.function.ret = function();) \
          (n->call.function.ret = function(CQ_EVAL(CQ_MAP_COMMA(CQ_RUN_CALL_ARG, function, 0, types)));) \
      ) \
    break;

static int cq_run_call(struct cq *context)
{
  struct cq_queue_node *n = cq_queue_pop_(&context->jobs);
  if (!n)
    return 0;

  switch(n->type)
  {
    #define CQ_FUNCTION CQ_RUN_CALL_BUILD
    #include CQ_DESCRIPTION_FILE
    #undef CQ_FUNCTION
  }

  if (n->push_result)
    cq_queue_push_(&context->results, n);
  else
    free(n);
  return 1;
}

static void cq_run_calls(struct cq *context)
{
  while(cq_run_call(context))
    ;
}

/* Get results */
#define CQ_RESULT_ARG(function, pos, ...) n->call.function.arg##pos
#define CQ_RESULT_BUILD(function, type_ret, types...) \
  case CQ_TYPE_##function: \
    CQ_IF_ELSE(CQ_IS_VOID(type_ret)) \
      (return NULL;)(return &n->call.function.ret); \
    break;

#define cq_result(context, return_type) \
  (*(return_type *) cq_result_ptr(context))

void *cq_result_ptr(struct cq *context)
{
  struct cq_queue_node *n = cq_queue_next_(&context->results);
  if (!n)
    return NULL;

  switch (n->type)
  {
    #define CQ_FUNCTION CQ_RESULT_BUILD
    #include CQ_DESCRIPTION_FILE
    #undef CQ_FUNCTION
    default: return NULL;
  }
}

void cq_pop_result(struct cq *context)
{
  struct cq_queue_node *n = cq_queue_pop_(&context->results);
  if (n)
    free(n);
}

void cq_clear_results(struct cq *context)
{
  struct cq_queue_node *n = cq_queue_pop_(&context->results);
  while(n)
  {
    free(n);
    n = cq_queue_pop_(&context->results);
  }
}

size_t cq_job_count(struct cq *context)
{
  return cq_queue_size_(&context->jobs);
}

size_t cq_result_count(struct cq *context)
{
  return cq_queue_size_(&context->results);
}

#endif /* #define _CQ_H */
