# Call Queue

CQ is a single C file utility providing an easy and safe way to asynchronously
enqueue function calls so they can be ran later or in a separate thread.

# The problem

To make a separate thread calling a function, a classical approach is to create
a structure with all arguments and a way to store the result:
```c
int my_function(int a, int b);

struct my_function_wrapper {
  int a;
  int b;
  int return_;
};
```
Then you will have to enqueue your calls in a thread-safe queue so they can be
ran later. You will probably need to check result and make another queue to get
the result back. With more functions, you will end up with a bloaty code
composed of `union`, `enums`, structures filling and your code will become
harder to maintain.

This tool address this problem by generating for you all this stuff using
a few macros. You will mainly need to describe your function prototypes in a
separate file like:
```
CQ_FUNCTION(my_function, int, int, int)
                          ^    ^    ^__ second argument type (int a)
                           \    \______ first argument type (int b)
                            \__________ function return type (int)
```
If your function signatures change, adapting your code will be minimal and
more pleasant to read. Check the
[example folder](https://gitlab.com/mojo42/cq/tree/master/examples).

# Interface

It is preferable to describe the interface here to have a clear view of what
you will need to use (`cq.h` is quite filled with preprocessor tricks).

___
```c
int cq_init(struct cq *context);
```
Initialize a new call queue.
- context: address to a `struct cq`
- return: `0` if all goes well

___
```c
int cq_destroy(struct cq *context);
```
Destroys a call queue.
- context: address to a `struct cq`
- return: `0` if all goes well

___
```c
void cq_push_call(struct cq *context, function, args...);
```
Pushes call in a queue
- context: address to a `struct cq`
- function: function to call
- args: list of your function arguments separated by commas

___
```c
void cq_push_call_no_res(struct cq *context, function, args...);
```
Same as `cq_push_call` but we specify that the result won't be stored in the
result queue.

___
```c
int cq_run_call(struct cq *context);
```
Runs the next call in job queue and store result in result queue (if asked).
- context: address to a `struct cq`
- return: 0 if there are no remaining jobs

___
```c
void cq_run_calls(struct cq *context);
```
Runs all calls in job queue and store results in result queue (if asked).
- context: address to a `struct cq`

___
```c
void *cq_result_ptr(struct cq *context);
```
Gives a pointer to the next job result.
You will need to manually call `cq_pop_result` or `cq_clear_results` to
remove job result from result queue.

Once job result is removed from result queue, pointer provided by
`cq_result_ptr` is not valid anymore.
- context: address to a `struct cq`
- return: pointer to the next result

___
```c
#define cq_result(context, return_type) \
  (*(return_type *) cq_result_ptr(context))
```
Convenient macro to cast `cq_result_ptr` result.
- context: address to a `struct cq`
- return_type: type of expected result.
- return: result of type return_type

___
```c
void cq_pop_result(struct cq *context);
```
Removes next job result from result queue.
To access the result, check `cq_result_ptr` before using `cq_pop_result`
- context: address to a `struct cq`

___
```c
void cq_clear_results(struct cq *context);
```
Removes all job results from result queue.
- context: address to a `struct cq`

___
```c
size_t cq_job_count(struct cq *context);
```
Get the number of pending jobs.
- context: address to a `struct cq`
- return: job queue size

___
```c
size_t cq_result_count(struct cq *context);
```
Get the number of pending results.
- context: address to a `struct cq`
- return: result queue size

# License

> Copyright (c) 2016, Jérôme Jutteau <j.jutteau@gmail.com>
>
> This work is open source software, licensed under the terms of the
> BSD license as described in the LICENSE file in the top-level directory.
